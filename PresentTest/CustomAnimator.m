//
//  CustomAnimator.m
//  PresentTest
//
//  Created by sunlong on 2018/7/20.
//  Copyright © 2018年 SunDe. All rights reserved.
//

#import "CustomAnimator.h"

@implementation CustomAnimator

//动画代理
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.5;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    if (self.isPresented == YES) {
        [self presentVC:transitionContext];
    }else{
        [self dismissVC:transitionContext];
    }
}

- (void)presentVC:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    [transitionContext.containerView addSubview:toView];
    toView.transform = CGAffineTransformMakeScale(1, 0);
        toView.layer.anchorPoint = CGPointMake(0.5, 0);
    
    [UIView animateWithDuration:0.5 animations:^{
        toView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

- (void)dismissVC:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    [transitionContext.containerView addSubview:fromView];
        fromView.layer.anchorPoint = CGPointMake(0.5, 0);
    
    [UIView animateWithDuration:0.5 animations:^{
        fromView.transform = CGAffineTransformMakeScale(1, 0.001);
        
    } completion:^(BOOL finished) {
        [fromView removeFromSuperview];
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}




@end
