//
//  YouwoPresentContainerVC.m
//  FunTown
//
//  Created by 彭冲 on 20/7/18.
//  Copyright © 2018年 FunTown. All rights reserved.
//

#import "YouwoPresentContainerVC.h"

@interface YouwoPresentContainerVC ()

{
    UIView *_blackBgView;
}

//@property (nonatomic, strong) UIView *blackBgView;

@end

@implementation YouwoPresentContainerVC

//重写容器方法
- (void)containerViewWillLayoutSubviews {
    
    [super containerViewWillLayoutSubviews];
    
    self.presentedView.frame = CGRectMake(0, 64, self.containerView.frame.size.width, 300);
    
    _blackBgView = [[UIView alloc] initWithFrame:self.containerView.bounds];
    _blackBgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.containerView insertSubview:_blackBgView atIndex:0];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenCardList)];
    [_blackBgView addGestureRecognizer:tap];
}

- (void)hiddenCardList{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
