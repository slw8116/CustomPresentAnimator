//
//  CustomAnimator.h
//  PresentTest
//
//  Created by sunlong on 2018/7/20.
//  Copyright © 2018年 SunDe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomAnimator : NSObject <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) BOOL isPresented;


@end
