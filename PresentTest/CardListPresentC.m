//
//  CardListPresentC.m
//  GeXiaZi
//
//  Created by 孙哈哈 on 2017/8/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "CardListPresentC.h"

#define SWidth [UIScreen mainScreen].bounds.size.width
#define SHeigth [UIScreen mainScreen].bounds.size.height

@interface CardListPresentC  ()

@property (nonatomic, strong) UIView *blackBGView;

@end

@implementation CardListPresentC

- (void)containerViewWillLayoutSubviews{
    
    [super containerViewWillLayoutSubviews];
    
    
    self.presentedView.frame = self.presentFrame;
    
    self.blackBGView = [[UIView alloc] initWithFrame:self.containerView.bounds];
    self.blackBGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.containerView insertSubview:self.blackBGView atIndex:0];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenCardList)];
    [self.blackBGView addGestureRecognizer:tap];

}

- (void)hiddenCardList{
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
