//
//  CardListPresentC.h
//  GeXiaZi
//
//  Created by 孙哈哈 on 2017/8/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardListPresentC : UIPresentationController

@property (nonatomic, assign) CGRect presentFrame;

@end
