//
//  ViewController.m
//  PresentTest
//
//  Created by sunlong on 2018/7/20.
//  Copyright © 2018年 SunDe. All rights reserved.
//

#import "ViewController.h"
#import "SecondVC.h"

#import "CustomAnimator.h"
#import "YouwoPresentContainerVC.h"

@interface ViewController () <UIViewControllerTransitioningDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor orangeColor];
    
    UIButton *aa = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    aa.backgroundColor = [UIColor redColor];
    [aa addTarget:self action:@selector(aaClick) forControlEvents:64];
    [self.view addSubview:aa];
}

- (void)aaClick {
    
    SecondVC *bb = [[SecondVC alloc] init];
    bb.modalPresentationStyle = UIModalPresentationCustom;
    bb.transitioningDelegate = self;
    [self presentViewController:bb animated:YES completion:nil];
}

//dlajdlajdfl

//转场代理
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {

    YouwoPresentContainerVC *containerVC = [[YouwoPresentContainerVC alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    return containerVC;
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    CustomAnimator *animate = [[CustomAnimator alloc] init];
    animate.isPresented = YES;
    return animate;
}
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {

    CustomAnimator *animate = [[CustomAnimator alloc] init];
    animate.isPresented = NO;
    return animate;

}
@end
